# Easy Edit #

UML editor created on express.js + backbone + marionette

### See demo ###

Deployed application available [here](https://easy-edit.herokuapp.com)

### Local db setup guide ###

* Install and run mondoDB
* Create new db 
* Set connection to local db here: `src/mongo/mongoose.js`
    example: `mongoose.connect("mongodb://localhost:27017/uml");`

### Remote db setup guide ###

* set environment variable for your IDE with key `MONGOLAB_URI`
* set value to `mongodb://user:secret@ds011369.mlab.com:11369/heroku_jfw7t8z1`
* Application available on [localhost:3000](localhost:3000)