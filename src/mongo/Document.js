var mongoose = require('./mongoose');
var shortid = require('shortid');

var schema = mongoose.Schema({
    hash: {type: String, default: shortid.generate},
    title: 'String',
    settings: {type: mongoose.Schema.Types.ObjectId, ref: 'Settings'},
    elements: [{type: mongoose.Schema.Types.ObjectId, ref: 'Element'}]
}, {
    collection: 'Document'
});

var Document = mongoose.model('Document', schema);

module.exports = Document;