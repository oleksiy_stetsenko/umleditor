var mongoose = require('./mongoose');

var schema = mongoose.Schema({
    width: Number,
    height: Number,
    lineType: String,
    lineColor: String,
    backgroundColor: String
},{
    collection: 'Settings'
});

var Settings = mongoose.model('Settings', schema);

module.exports = Settings;
