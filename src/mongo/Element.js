var mongoose = require('./mongoose');

var schema = mongoose.Schema({
    type: 'String',
    elementSettings: 'Object',
    generalSettings: 'Object',
    position: 'Number'
}, {
    collection: 'Element'
});

var Element = mongoose.model('Element', schema);

module.exports = Element;