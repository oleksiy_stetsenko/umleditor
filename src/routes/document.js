var express = require('express');
var router = express.Router();
var Document = require('../mongo/Document');
var Settings = require('../mongo/Settings');

router.route('/:hash')
    .get(function (req, res) {
        Document
            .findOne({hash: req.params.hash})
            .populate('settings')
            .populate('elements')
            .exec(function (err, document) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                res.json(document);
            });
    })
    .put(function (req, res) {
        Document.findOne({hash: req.params.hash}, function (err, item) {
            if (err) {
                console.log("Error retrieving item from db");
                res.send(500, err);
                return;
            }
            item.title = req.body.title;
            item.save(function (err) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                res.sendStatus(200);
            });
        });
    });

router.route('/')
    .get(function (req, res) {
        Document
            .find()
            .exec(function (err, document) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                res.json(document);
            });
    })
    .post(function (req, res) {
        var document = new Document(req.body);
        var settings = new Settings(req.body.settings);

        settings.save();

        document.settings = settings;
        document.save(function (err) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.status(201).json(document);
        });
    });

module.exports = router;
