var express = require('express');
var router = express.Router();
var Document = require('../mongo/Document');
var Element = require('../mongo/Element');

router.route('/:hash')
    .get(function (req, res) {
        Document.findOne({hash: req.param.hash}, function (err, document) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.json(document.elements);
        });
    });

router.route('/:hash')
    .post(function (req, res) {

        var element = new Element(req.body);
        element.type = req.body.type;
        element.generalSettings = req.body.generalSettings;
        element.elementSettings = req.body.elementSettings;
        element.position = req.body.position;
        element.save();

        Document
            .findOne({hash: req.params.hash}, function (err, document) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                document.elements.push(element);
                document.save();
                res.status(201).json(element);
            });
    });

router.route('/:hash/:id')
    .put(function (req, res) {
        Document
            .findOne({hash: req.params.hash})
            .populate('elements')
            .exec(function (err, document) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                Element
                    .findOne({_id: req.params.id})
                    .exec(function (err, element) {
                        if (err) {
                            console.error(err);
                            return res.sendStatus(500);
                        }
                        element.type = req.body.type;
                        element.generalSettings = req.body.generalSettings;
                        element.elementSettings = req.body.elementSettings;
                        element.position = req.body.position;
                        element.save();
                        res.sendStatus(200);
                    });
            });
    })
    .delete(function (req, res) {
        Document
            .update(
                {hash: req.params.hash},
                {$pull: {elements: req.params.id}}, function (err) {
                    if (err) {
                        console.log(err);
                        return res.sendStatus(500);
                    }
                    Element
                        .findOneAndRemove({_id: req.params.id}, function (err) {
                            if (err) {
                                console.error(err);
                                return res.sendStatus(500);
                            }
                            res.sendStatus(200);
                        });
                });
    });

module.exports = router;
