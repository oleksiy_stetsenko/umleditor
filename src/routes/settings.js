var express = require('express');
var router = express.Router();
var Document = require('../mongo/Document');
var Settings = require('../mongo/Settings');

router.route('/:hash')
    .put(function (req, res) {
        Document.findOne({hash: req.params.hash}, function (err, item) {
            if (err) {
                console.log("Error retrieving item from db");
                return res.send(500, err);
            }
            Settings
                .findOne({_id: item.settings}, function (err, settings) {
                    settings.width = req.body.width;
                    settings.height = req.body.height;
                    settings.lineType = req.body.lineType;
                    settings.lineColor = req.body.lineColor;
                    settings.backgroundColor = req.body.backgroundColor;
                    settings.save(function (err) {
                        if (err) {
                            console.error(err);
                            return res.sendStatus(500);
                        }
                        return res.sendStatus(200);
                    });
                });
        });
    });

module.exports = router;