define([
    'backbone.marionette',
    './template'
], function (Marionette, template) {
    return Marionette.LayoutView.extend({
        el: document.body,
        template: template,

        regions: {
            header: '.application-header',
            content: '.application-content'
        }
    });
});