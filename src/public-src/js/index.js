require.config({
    baseUrl: './js',
    shim : {
        "bootstrap" : { "deps" :['jquery'] },
        "colorpicker": { "deps" : ['jquery', 'bootstrap'] },
        "selectize": { "deps" : ['jquery'] }
    },
    paths: {
        'jquery': './libs/jquery',
        'backbone': './libs/backbone',
        'backbone.marionette': './libs/backbone.marionette',
        'underscore': './libs/underscore',
        'colorpicker': './libs/bootstrap-colorpicker.min',
        'selectize': './libs/selectize.min',
        'bootstrap': './libs/bootstrap.min'
    }
});

require([
    'jquery',
    'backbone',
    './application/Application',
    './main/Router',
    'colorpicker',
    'selectize',
    'bootstrap'

], function ($, Backbone, Application, MainRouter) {

    var app = new Application();

    new MainRouter({
        container: {
            header: app.layout.header,
            content: app.layout.content
        }
    });

    $(function () {
        app.on('start', function () {
            Backbone.history.start();
        });

        app.start();
    });
});
