define([
    'backbone.marionette',
    '../../content/contentSvg/ElementService/Service',
    './template',
    'jquery',
    'selectize',
    'bootstrap',
    'colorpicker'

], function (Marionette, Service, template) {
    return Marionette.ItemView.extend({

        tagName: 'ul',
        className: 'general-settings row middle-xs',
        template: template,

        ui: {
            lineColor: '#color-line',
            colorLine: '#line-color',
            bgColor: '#color-bg',
            colorBg: '#bg-color',
            lineType: '#select-line-type',
            selectLineType: '#select-line-type',
            paperSize: '#paper-size',
            tooltip: '[data-toggle="tooltip"]',
            nameDoc: '#app-name'
        },

        events: {
            'change @ui.lineType': 'changeLineType',
            'changeColor.colorpicker @ui.colorLine': 'changeLineColor',
            'changeColor.colorpicker @ui.colorBg': 'changeBackgroundColor',
            'change @ui.paperSize': 'changePaperSize'
        },

        initialize: function () {
            this.listenTo(this.model, "change", this.modelChange);
        },

        modelChange: function () {
            if (_.has(this.model.changed, 'hash')) {
                this.destroyPicker();
                this.render();
                this.onShow();

            } else {
                if (!this.model.isNew()) {
                    this.model.url = 'settings/' + this.model.get('hash');
                    this.model.save();
                }
            }
        },

        onShow: function () {
            this.ui.colorLine.colorpicker();
            this.ui.colorBg.colorpicker();
            this.ui.selectLineType.selectize();
            this.ui.paperSize.selectize();
            this.ui.tooltip.tooltip();
        },

        destroyPicker: function () {
            this.ui.colorLine.colorpicker('destroy');
            this.ui.colorBg.colorpicker('destroy');
        },

        changeLineType: function () {
            this.model.set("lineType", this.ui.selectLineType.val());
        },

        changeLineColor: function () {
            this.model.set("lineColor", this.ui.lineColor.val());
        },

        changeBackgroundColor: function () {
            this.model.set("backgroundColor", this.ui.bgColor.val());
        },

        changePaperSize: function () {
            var newWidth = Service.convertSize(this.ui.paperSize.val()).width;
            var newHeight = Service.convertSize(this.ui.paperSize.val()).height;
            this.model.set("width", newWidth);
            this.model.set("height", newHeight);
            if (!this.model.isNew())
                SVG.get('svg-project').size(newWidth, newHeight);
        }
    })
});