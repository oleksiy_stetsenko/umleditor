define([
    'backbone.marionette',
    './template'
], function (Backbone, template) {
    return Backbone.LayoutView.extend({
        template: template,

        regions: {
            mainButtons: '.app-name.row.middle-xs',
            docSet: '.menu-bar.row.middle-xs',
            modal: '.application-modal'
        }
    });
});
