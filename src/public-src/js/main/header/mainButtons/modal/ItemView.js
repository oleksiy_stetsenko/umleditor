define([
    'backbone',
    'backbone.marionette',
    './template'

], function (Backbone, Marionette, template) {
    return Marionette.ItemView.extend({
        tagName: 'li',
        template: template,

        initialize: function () {
            this.render();
        }
    });
});
