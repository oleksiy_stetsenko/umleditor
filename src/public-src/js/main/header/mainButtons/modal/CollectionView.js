define([
    'backbone.marionette',
    './collection-template',
    './ItemView'

], function (Marionette, template, ItemView) {
    return Marionette.CompositeView.extend({
        template: template,
        childView: ItemView,
        childViewContainer: '.project-list',

        ui: {
            modalWindow: '#openModal',
            projectList: '.project-list',
            projectItem: '.project-list li',
            activeProjectItem: '.project-list .active',
            submitButton: 'button[data-action=submit]',

            newItem: '#new',
            openItem: '#open'
        },

        events: {
            'click span[data-action=close]': 'close',
            'click @ui.submitButton': 'submit',
            'click @ui.projectItem': 'active',
            'click @ui.newItem' : 'newItemClick',
            'click @ui.openItem' : 'openItemClick'
        },

        newItemClick: function () {
            this.ui.projectList[0].style.display = 'none';
            this.ui.submitButton[0].removeAttribute("disabled");

            this.ui.openItem[0].removeAttribute("checked");
            this.ui.newItem[0].setAttribute('checked', true);

        },

        openItemClick: function () {
            this.ui.submitButton[0].disabled = true;
            this.ui.projectList[0].style.display = 'block';
            this.ui.newItem[0].removeAttribute("checked");
            this.ui.openItem[0].setAttribute('checked', true);
        },

        active: function (e) {
            this.ui.submitButton[0].removeAttribute("disabled");

            this.ui.projectList.find('li').removeClass("active");
            if ($(e.target).is('li')) {
                $(e.target).addClass('active');
            } else {
                $(e.target).parent().addClass('active');
            }
        },

        submit: function () {
            if (this.ui.openItem[0].checked) {
                var hash = this.ui.projectList.find('.active').children('h4')[0].dataset.hash;
                this.model.set('hash', hash, {silent: true});
                this.model.trigger('openProject');
            } else {
                this.model.unset('hash', {silent: true});
                this.model.trigger('createProject');
            }

            this.close();
        },

        close: function () {
            this.ui.modalWindow[0].style.display = "none";
        }
    });
});