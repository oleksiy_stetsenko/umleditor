define([
    'backbone.marionette',
    '../../../libs/svg.min'

], function (Marionette) {
    var Service = Marionette.Object.extend({

        createProject: function (model) {
            this._eraseModel(model);

            model.set('title', model.defaults.title);
            model.set('settings', model.settings.getDefaultValue(), {silent: true});
            model.settings.attributes = model.settings.getDefaultValue();
            model.url = 'document';
            model.save()
        },

        openProject: function (model) {
            var hash = model.get('hash');
            this._eraseModel(model);
            model.url = 'document/' + hash;
            model.fetch();
        },

        _eraseModel: function (model) {
            model.clear({silent: true});
            model.settings.clear({silent: true});
            model.elements.reset();
            delete model.elements._events;
        }
    });
    return new Service;
});

