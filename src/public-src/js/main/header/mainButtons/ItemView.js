define([
    'backbone',
    'backbone.marionette',
    '../../content/contentSvg/CollectionView',
    './template',
    './CreateService',
    './modal/CollectionView',
    '../../../document/Collection',
    '../../../libs/svg.min',
    '../../../libs/jquery',
    '../../../libs/bootstrap.min'


], function (Backbone, Marionette, CollectionView, template, CreateService, ModalView, Collection) {
    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        collection: new Collection,
        className: 'app-wrap row middle-xs',
        paper: null,

        ui: {
            nameDoc: '#app-name',
            newDoc: '#new-file',
            openDoc: '#open-file',
            deleteElement: '#delete',
            copyElement: '#copy',
            forwardElement: '#forward',
            backElement: '#back',
            contentSvg: '#svg-content',
            tooltip: '[data-toggle="tooltip"]'
        },

        events: {
            'change @ui.nameDoc': 'changeNameDoc',
            'click @ui.newDoc': 'createIconClick',
            'click @ui.openDoc': 'openIconClick',
            'click @ui.copyElement': 'copyEl',
            'click @ui.deleteElement': 'deleteEl',
            'click @ui.forwardElement': 'forward',
            'click @ui.backElement': 'back'
        },

        initialize: function (options) {
            this.container = options.container;
            this.listenTo(this.model, "change:title", this.render);
            this.model.on('createProject', this.createProject, this);
            this.model.on('openProject', this.openProject, this);
            this.model.on('keyCreateProject', this.createIconClick, this);
            this.model.on('keyOpenProject', this.openIconClick, this);
        },

        onShow: function () {
            this.ui.tooltip.tooltip();
        },

        changeNameDoc: function () {
            this.model.set("title", this.ui.nameDoc.val());
            if (!this.model.isNew()) {
                this.model.url = 'document/' + this.model.get('hash');
                this.model.save();
            }
        },

        createIconClick: function () {
            CreateService.createProject(this.model);
            this.redrawView();
        },

        openIconClick: function () {
            this.collection.fetch();
            var modalView = new ModalView({
                model: this.model,
                collection: this.collection
            });
            this.container.show(modalView);
        },

        redrawView: function () {
            if (this.container) {
                this.container.destroy();
            }
            if (this.collectionView) {
                this.collectionView.destroy();
            }
            this.collectionView = new CollectionView({model: this.model});
        },

        createProject: function () {
            this.model.url = 'document/';
            this.model.save();
            this.redrawView();
        },

        openProject: function () {
            CreateService.openProject(this.model);
            this.redrawView();
        },

        currentElement: function () {
            return this.model.elements.findWhere({select: 'current'});
        },
        copyEl: function () {
            this.currentElement().trigger('copy');
        },
        deleteEl: function () {
            this.currentElement().trigger('delete');
        },
        forward: function () {
            this.currentElement().trigger('forward');
        },
        back: function () {
            this.currentElement().trigger('back');
        }
    });
});
