define([
    'backbone.marionette',
    '../../../document/elements/Model',
    './ElementService/Select',
    './ElementService/Service',
    '../../../libs/svg.min',
    '../../../libs/svg.draggable.min'


], function (Marionette, ModelElement, Select, Service) {
    return Marionette.ItemView.extend({
        el: '#svg-project',
        template: false,

        initialize: function () {

            this.modelElement = this.currentModel();
            this.paper = SVG.get('svg-project');
            this.figureGroup = SVG.get('figure');
            this.element = this.figureGroup[this.modelElement.get('type')]().stroke({ width: 2 });
            this.element
                .attr(Service.convertAttributes(this.modelElement.get('generalSettings')))
                .attr(this.modelElement.get('elementSettings'));
            this.limitDrag(this.element, this.paper.width(), this.paper.height());

            var positElement = this.modelElement.get('position');
            if (positElement !== undefined){
                while(positElement !== this.element.position() && positElement < this.element.siblings().length){
                    if (positElement > this.element.position()){
                        this.element.forward()
                    }else this.element.backward()
                }
            }

            if (this.modelElement.get('status') == 'cloned'){
                this.element.dmove(10,10);
                this.modelElement.unset('status', {silent: true});
                Service.updateModel(this.modelElement, this.element.bbox())
            }

            this.paper.mousedown(function (e) {
                e.preventDefault();
                if (this.currentModel()) {
                    this.currentModel().set('select', 'unselect');
                }
                this.clearGroups();
            }.bind(this));

            this.element.mousedown(function (e) {
                e.preventDefault();
                this.clearGroups();
                var selectedModel = this.currentModel();
                if (selectedModel){
                    selectedModel.set('select', 'unselect');
                }
            }.bind(this));

            this.element.mouseup(function (e) {
                e.preventDefault();
                Select.selectElement(this.modelElement, this.element);
                Service.updateModel(this.modelElement, this.element.bbox());
                this.modelElement.set('select', 'current');
            }.bind(this));

            this.clearGroups();
            Select.selectElement(this.modelElement, this.element);

            this.modelElement.on("updateElement", this.changeElementAttr, this);
            this.modelElement.on("copy", this.copyElement, this);
            this.modelElement.on("delete", this.deleteElement, this);
            this.modelElement.on("forward", this.forwardElement, this);
            this.modelElement.on("back", this.backElement, this);
            this.model.settings.on("change", function () {
                this.figureGroup.each(function(i, children){
                    var newWidth = this.model.settings.get('width');
                    var newHeight = this.model.settings.get('height');
                    children[i].draggable(false);
                    this.limitDrag(children[i], newWidth, newHeight);
                }.bind(this))
            }, this);

            $(document).unbind('keydown');
            _.bindAll(this, 'onKeydown');
            $(document).bind('keydown', this.onKeydown);
        },

        changeElementAttr: function () {
            this.clearGroups();
            this.element
                .attr(Service.convertAttributes(this.currentModel().get('generalSettings')))
                .attr(this.currentModel().get('elementSettings'));
            Select.selectElement(this.currentModel(), this.element);
            Service.updateModel(this.currentModel(), this.element.bbox());
        },

        limitDrag: function (element, width, height) {
            element.draggable({
                minX: 0,
                minY: 0,
                maxX: width,
                maxY: height
            });
        },

        clearGroups: function () {
            SVG.get('sel-square').clear();
            SVG.get('sel-points').clear();
        },

        onKeydown: function(e) {
            if (e.altKey && e.keyCode == 78) {
                this.model.trigger('keyCreateProject');
            }
            if (e.altKey && e.keyCode == 79) {
                this.model.trigger('keyOpenProject');
            }
            if (e.altKey && e.keyCode == 67) {
                this.currentModel().trigger('copy');
            }
            if (e.keyCode == 46) {
                this.currentModel().trigger('delete');
            }
            if (e.altKey && e.keyCode == 38) {
                this.currentModel().trigger('forward');
            }
            if (e.altKey && e.keyCode == 40) {
                this.currentModel().trigger('back');
            }
        },

        currentModel: function () {
            return this.model.elements.findWhere({select: 'current'});
        },

        copyElement: function () {
            var currentModel = this.currentModel();
            currentModel.set('select', 'unselect');
            var modelElement = new ModelElement;
            modelElement.set('generalSettings', currentModel.get('generalSettings'));
            modelElement.set('elementSettings', currentModel.get('elementSettings'));
            modelElement.set('type', currentModel.get('type'));
            modelElement.set('hash', currentModel.get('hash'));
            modelElement.set('position', this.model.elements.length);
            modelElement.url = 'elements/' + modelElement.get('hash');
            modelElement.save()
                .done(function (data) {
                    modelElement.set('id', data._id);
                    modelElement.set('status', 'cloned');
                    this.model.elements.add(modelElement);
                }.bind(this));
        },

        forwardElement: function () {
            var thisModel = this.currentModel();
            var thisPosition = thisModel.get('position');
            var nextModel = this.model.elements.findWhere({position: (thisPosition + 1)});

            if (thisPosition < (this.model.elements.length - 1)) {
                this.element.forward();

                nextModel.set('position', thisPosition);
                Service.setUrl(nextModel);
                nextModel.save();

                thisModel.set('position', (thisPosition + 1));
                Service.setUrl(thisModel);
                thisModel.save();
            }
        },


        backElement: function () {
            var thisModel = this.currentModel();
            var thisPosition = thisModel.get('position');
            var nextModel = this.model.elements.findWhere({position: (thisPosition - 1)});

            if (thisPosition > 0) {
                this.element.backward();

                nextModel.set('position', thisPosition);
                Service.setUrl(nextModel);
                nextModel.save();

                thisModel.set('position', (thisPosition - 1));
                Service.setUrl(thisModel);
                thisModel.save();
            }
        },

        deleteElement: function () {
            if (this.modelElement.get('select') == 'current') {
                this.modelElement.set('select', 'unselect');
                var pos = this.modelElement.get('position');
                Service.setUrl(this.modelElement);
                this.modelElement.destroy();
                this.model.elements.remove(this.modelElement);
                this.clearGroups();
                this.element.remove();
                var quantityElements = this.model.elements.length;
                for (var i = pos; i < quantityElements; i++){
                    var model = this.model.elements.findWhere({position: i + 1});
                    Service.setUrl(model);
                    model.set('position', i);
                    model.save();
                }
            }
        }
    });
});