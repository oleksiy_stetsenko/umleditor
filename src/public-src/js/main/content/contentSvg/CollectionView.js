define([
    'backbone.marionette',
    './ItemView',
    '../rightSidebar/ItemView',
    '../../../libs/svg.min'

], function (Marionette, ElementView, RightSidebarView) {
    return Marionette.CompositeView.extend({
        el: '#svg-project',
        template: false,
        childView: ElementView,
        childViewContainer: '#figure',

        initialize: function () {
            this.paper = SVG('svg-content').size(this.model.settings.get('width'), this.model.settings.get('height'));
            this.paper.attr('id', 'svg-project');
            this.paper.group().attr('id', 'figure');
            this.paper.group().attr('id', 'sel-square');
            this.paper.group().attr('id', 'sel-points');

            this.model.settings.on('change', this.updateSvgPageSize, this);
            this.model.elements.on('add', this.addElementView, this);
            this.model.elements.on("change:select", this.selectOrUnSelectElement, this);
            this.on('before:destroy', this.clearSvg);
        },

        updateSvgPageSize: function () {
            if (!_.isUndefined(this.model.settings)) {
                this.paper.size(this.model.settings.get('width'), this.model.settings.get('height'));
            }
        },

        selectOrUnSelectElement: function (item) {
            if (_.isUndefined(item.changed.select) || item.changed.select === 'unselect' && this.rightSidebarView) {
                this.rightSidebarView.destroy();
                $('.edit-list a').addClass('disabled');
            } else {
                this._createSelectedElementView(item);
                $('.edit-list a').removeClass('disabled');
            }
        },

        _createSelectedElementView: function (item) {
            $('.elemPicker').remove();
            this.rightSidebarView = new RightSidebarView({
                model: item,
                docModel: this.model
            });
        },

        addElementView: function (item) {
            var selectedModel = this.model.elements.findWhere({select: 'current'});
            if (selectedModel) {
                selectedModel.set('select', 'unselect');
            }
            item.set('select', 'current');
            new this.childView({model: this.model});
        },

        clearSvg: function () {
            this.paper.node.remove();
        }
    });
});

