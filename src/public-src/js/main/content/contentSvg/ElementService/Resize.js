define([
    'backbone.marionette',
    '../../../../libs/svg.min',
    '../../../../libs/svg.draggable.min'
], function (Marionette) {
    var Resize = Marionette.Object.extend({

        resizeElement: function(point, figure, selSquare) {

            function getCentralPointForEllipse(point, figure) {
                return {
                    tl: function (figure) {
                        return {
                            x: figure.cx.baseVal.value + figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value + figure.ry.baseVal.value
                        };
                    },
                    tr: function (figure) {
                        return {
                            x: figure.cx.baseVal.value - figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value + figure.ry.baseVal.value
                        };
                    },
                    bl: function (figure) {
                        return {
                            x: figure.cx.baseVal.value + figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value - figure.ry.baseVal.value
                        };
                    },
                    br: function (figure) {
                        return {
                            x: figure.cx.baseVal.value - figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value - figure.ry.baseVal.value
                        };
                    },
                    tm: function (figure) {
                        return {
                            x: figure.cx.baseVal.value - figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value + figure.ry.baseVal.value,
                            middle: 'top'
                        };
                    },
                    ml: function (figure) {
                        return {
                            x: figure.cx.baseVal.value + figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value - figure.ry.baseVal.value,
                            middle: 'left'
                        };
                    },
                    mr: function (figure) {
                        return {
                            x: figure.cx.baseVal.value - figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value - figure.ry.baseVal.value,
                            middle: 'right'
                        };
                    },
                    bm: function (figure) {
                        return {
                            x: figure.cx.baseVal.value - figure.rx.baseVal.value,
                            y: figure.cy.baseVal.value - figure.ry.baseVal.value,
                            middle: 'bottom'
                        };
                    }
                }[point.id](figure);
            }

            function getCentralPointForRect(point, figure) {
                return {
                    tl: function (figure) {
                        return {
                            x: figure.x.baseVal.value + figure.width.baseVal.value,
                            y: figure.y.baseVal.value + figure.height.baseVal.value
                        };
                    },
                    tr: function (figure) {
                        return {
                            x: figure.x.baseVal.value,
                            y: figure.y.baseVal.value + figure.height.baseVal.value
                        };
                    },
                    bl: function (figure) {
                        return {
                            x: figure.x.baseVal.value + figure.width.baseVal.value,
                            y: figure.y.baseVal.value
                        };
                    },
                    br: function (figure) {
                        return {
                            x: figure.x.baseVal.value,
                            y: figure.y.baseVal.value
                        };
                    },
                    tm: function (figure) {
                        return {
                            x: figure.x.baseVal.value,
                            y: figure.y.baseVal.value + figure.height.baseVal.value,
                            middle: 'top'
                        };
                    },
                    ml: function (figure) {
                        return {
                            x: figure.x.baseVal.value + figure.width.baseVal.value,
                            y: figure.y.baseVal.value,
                            middle: 'left'
                        };
                    },
                    mr: function (figure) {
                        return {
                            x: figure.x.baseVal.value,
                            y: figure.y.baseVal.value,
                            middle: 'right'
                        };
                    },
                    bm: function (figure) {
                        return {
                            x: figure.x.baseVal.value,
                            y: figure.y.baseVal.value,
                            middle: 'bottom'
                        };
                    }
                }[point.id](figure);
            }

            var editedFigure = (typeof selSquare === 'undefined') ? figure : selSquare;

            var editFigure = {
                ellipse: function () {
                    var center = getCentralPointForEllipse(point, figure);
                    (typeof selSquare === 'undefined')
                        ? editEllipse(point, center, editedFigure)
                        : editRectangle(point, center, editedFigure);
                },
                rect: function () {
                    var center = getCentralPointForRect(point, figure);
                    editRectangle(point, center, editedFigure);
                },
                line: function () {
                    editLine(point, figure);
                }
            };
            editFigure[figure.nodeName](point, figure);

            function editEllipse(point, center, figure) {
                figure.cx.baseVal.value = (center.middle === 'top' || center.middle === 'bottom')
                    ? point.cx.baseVal.value - point.r.baseVal.value
                    : (center.x + point.cx.baseVal.value) / 2;
                figure.rx.baseVal.value = (center.middle === 'top' || center.middle === 'bottom')
                    ? (point.cx.baseVal.value - center.x - point.r.baseVal.value)
                    : Math.abs((center.x - point.cx.baseVal.value) / 2);
                figure.cy.baseVal.value = (center.middle === 'left' || center.middle === 'right')
                    ? point.cy.baseVal.value - point.r.baseVal.value
                    : (center.y + point.cy.baseVal.value) / 2;
                figure.ry.baseVal.value = (center.middle === 'left' || center.middle === 'right')
                    ? Math.abs(center.y - point.cy.baseVal.value) - point.r.baseVal.value
                    : Math.abs((center.y - point.cy.baseVal.value) / 2);
            }

            function editLine(point, figure) {
                return {
                    t1: function (point, figure) {
                        figure.x1.baseVal.value = point.cx.baseVal.value;
                        figure.y1.baseVal.value = point.cy.baseVal.value;
                    },
                    t2: function (point, figure) {
                        figure.x2.baseVal.value = point.cx.baseVal.value;
                        figure.y2.baseVal.value = point.cy.baseVal.value;
                    }
                }[point.id](point, figure);
            }

            function editRectangle(point, center, figure) {
                figure.x.baseVal.value = (point.cx.baseVal.value <= center.x)
                    ? point.cx.baseVal.value
                    : center.x;
                figure.y.baseVal.value = (point.cy.baseVal.value <= center.y)
                    ? point.cy.baseVal.value
                    : center.y;
                figure.width.baseVal.value = (center.middle === 'top' || center.middle === 'bottom')
                    ? Math.abs(point.cx.baseVal.value - center.x - point.r.baseVal.value) * 2
                    : Math.abs(point.cx.baseVal.value - center.x);
                figure.height.baseVal.value = (center.middle === 'left' || center.middle === 'right')
                    ? Math.abs(point.cy.baseVal.value - center.y - point.r.baseVal.value) * 2
                    : Math.abs(point.cy.baseVal.value - center.y);
            }
        }
    });
    return new Resize;
});
