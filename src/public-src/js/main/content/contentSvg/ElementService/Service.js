define([
    'backbone.marionette',
    '../../../../libs/svg.min',
    '../../../../libs/svg.draggable.min'
], function (Marionette) {
    var Service = Marionette.Object.extend({

        lineConvert: function(line) {
                    switch (line) {
                        case 'solid':
                            return '0';
                            break;
                        case  'dashed':
                            return '5 5';
                            break;
                        case 'dotted':
                            return '2 3';
                            break;
                    }
        },

        convertSize: function (size) {
            switch (size) {
                case 'a4':
                    return {
                        width: 825,
                        height: 1168
                    };
                    break;
                case 'a3':
                    return {
                        width: 1168,
                        height: 1651
                    };
                    break;
                case 'a5':
                    return {
                        width: 583,
                        height: 825
                    };
                    break;
            }
        },

        convertSettingsForElement: function (attributes) {
            return {
                strokeDasharray: attributes.lineType,
                stroke: attributes.lineColor,
                fill: attributes.backgroundColor

            }
        },

        convertAttributes: function (attributes) {
            return {
                'stroke-dasharray': this.lineConvert(attributes.strokeDasharray),
                stroke: attributes.stroke,
                fill: attributes.fill
            }
        },

        updateModel: function (modelElement, elementAttr) {

            var keys = {
                rect: ['x', 'y', 'width', 'height'],
                ellipse: ['cx', 'cy'],
            };
            var elementSettings = modelElement.attributes.elementSettings;
            switch (modelElement.get('type')){
                case 'rect':
                    keys.rect.forEach(function (key) {
                        elementSettings[key] = elementAttr[key];
                    });
                    break;
                case 'ellipse':
                    keys.ellipse.forEach(function (key) {
                        elementSettings[key] = elementAttr[key];
                    });
                    elementSettings['rx'] = elementAttr['w']/2;
                    elementSettings['ry'] = elementAttr['h']/2;
                    break;
                case 'line':
                    elementSettings['x1'] = elementAttr['x'];
                    elementSettings['y1'] = elementAttr['y2'];
                    elementSettings['x2'] = elementAttr['x2'];
                    elementSettings['y2'] = elementAttr['y'];
                    break;
            }
            this.setUrl(modelElement);
            modelElement.save()
        },

        setUrl: function (modelElement) {
            modelElement.url = 'elements/' + modelElement.get('hash') + '/' + modelElement.get('id');
        }
    });
    return new Service;
});

