define([
    'backbone.marionette',
    './Resize',
    './Service',
    '../../../../libs/svg.min',
    '../../../../libs/svg.draggable.min'

], function (Marionette, Resize, Service) {
    var Select = Marionette.Object.extend({
        selectElement: function (model, element) {

            var paper = SVG.get('svg-project');
            var selSquareGroup = SVG.get('sel-square');
            var selPointsGroup = SVG.get('sel-points');
            var _me = this;
            var paperSize = {
                width: paper.width(),
                height: paper.height()
            };

            var r = 10;
            var markerStyle = {
                fill: '#28b9ee',
                stroke: '#999c9d'
            };

            function point(x, y) {
                return selPointsGroup.circle(r).attr({cx: x, cy: y}).attr(markerStyle);
            }

            function addSelectPointsAndSquare(param) {
                var selSquare = selSquareGroup.rect(param.w, param.h).move(param.x, param.y);
                selSquare.attr({
                    transform: 'translate(0.5, 0.5)',
                    "stroke-dasharray": '2 2',
                    stroke: '#000000',
                    fill: 'none'
                });

                var selPoints = {
                    tl: point(param.x, param.y).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0) && (x < paperSize.width),
                                y: (y > 0) && (y < paperSize.height)
                            };
                        }
                    ),
                    tr: point(param.x2, param.y).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0) && (x < paperSize.width),
                                y: (y > 0) && (y < paperSize.height)
                            };
                        }
                    ),
                    bl: point(param.x, param.y2).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0) && (x < paperSize.width),
                                y: (y > 0) && (y < paperSize.height)
                            };
                        }
                    ),
                    br: point(param.x2, param.y2).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0) && (x < paperSize.width),
                                y: (y > 0) && (y < paperSize.height)
                            };
                        }
                    ),
                    ml: point(param.x, param.cy).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0) && (x < paperSize.width),
                                y: param.cy
                            };
                        }),
                    mr: point(param.x2, param.cy).draggable(
                        function (x, y) {
                            return {
                                x: (x > 0 ) && (x < paperSize.width),
                                y: param.cy
                            };
                        }),
                    tm: point(param.cx, param.y).draggable(
                        function (x, y) {
                            return {
                                x: param.cx,
                                y: (y > 0) && (y < paperSize.height)
                            };
                        }),
                    bm: point(param.cx, param.y2).draggable(
                        function (x, y) {
                            return {
                                x: param.cx,
                                y: (y > 0) && (y < paperSize.height)
                            };
                        })
                };
                Object.keys(selPoints).forEach(function (point) {

                    selPoints[point].attr({'id': point});

                    selPoints[point].node.addEventListener('mousedown', function (e) {
                        e.preventDefault();
                        Object.keys(selPoints).forEach(function (thisPoint) {
                            if (point !== thisPoint) {
                                selPoints[thisPoint].remove();
                            }
                        });
                    });

                    selPoints[point].node.addEventListener('dragend', function (e) {
                        e.preventDefault();
                        Resize.resizeElement(this, element.node);
                        _me.selectElement(model, element);
                        Service.updateModel(model, element.bbox());
                    });

                    selPoints[point].node.addEventListener('dragmove', function (e) {
                        e.preventDefault();
                        Resize.resizeElement(selPoints[point].node, element.node, selSquare.node);
                    });
                });
            }

            function addSelectLine() {
                var selSquare = selSquareGroup.line(
                    element.node.x1.baseVal.value, element.node.y1.baseVal.value,
                    element.node.x2.baseVal.value, element.node.y2.baseVal.value);
                selSquare.attr({"stroke-dasharray": '2 2', stroke: '#000000'});
                var selPointsLine = {
                    t1: point(element.node.x1.baseVal.value, element.node.y1.baseVal.value).draggable(),
                    t2: point(element.node.x2.baseVal.value, element.node.y2.baseVal.value).draggable()
                };
                Object.keys(selPointsLine).forEach(function (point) {
                    selPointsLine[point].attr({'id': point});
                    selPointsLine[point].node.addEventListener('mouseup', function (e) {
                        e.preventDefault();
                        Resize.resizeElement(this, element.node);
                        _me.selectElement(model, element);
                        Service.updateModel(model, element.bbox());
                    });
                    selPointsLine[point].node.addEventListener('dragmove', function (e) {
                        e.preventDefault();
                        Resize.resizeElement(selPointsLine[point].node, element.node, selSquare.node);
                    });
                });
            }

            selPointsGroup.clear();
            selSquareGroup.clear();

            if (element.type == 'rect') {
                var rectParam = element.bbox();
                addSelectPointsAndSquare(rectParam)
            }

            if (element.type == 'ellipse') {
                var ellipseParam = element.bbox();
                addSelectPointsAndSquare(ellipseParam)
            }

            if (element.type == 'line') {
                addSelectLine();
            }
        }
    });
    return new Select;
});