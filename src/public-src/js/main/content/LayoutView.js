define([
    'backbone.marionette',
    './template'
], function (Backbone, template) {
    return Backbone.LayoutView.extend({
        template: template,

        regions: {
            leftBar: '.left-bar',
            rightBar: '.right-bar',
            svgContent: '#svg-content.center-xs'
        }
    });
});
