define([
    'backbone.marionette',
    './../../../document/elements/Model',
    './../contentSvg/ItemView',
    '../contentSvg/ElementService/Service',
    './template',
    '../../../libs/svg.min'

], function (Marionette, ModelElement, ViewElement, Service, template) {
    return Marionette.ItemView.extend({

        template: template,

        ui: {
            'buttonEllipse':'#ellipse',
            'buttonRect':'#rect',
            'buttonLine':'#line'
        },

        events: {
            'click @ui.buttonEllipse': 'createElement',
            'click @ui.buttonRect': 'createElement',
            'click @ui.buttonLine': 'createElement'
        },

        createElement: function (event) {

            var typeElement = event.currentTarget.id;
            var elementSettings = this.model.getElementDefaults()[typeElement];
            var element = new ModelElement;
            element.set('type', typeElement);
            element.set('generalSettings', {
                strokeDasharray: this.model.settings.get('lineType'),
                stroke: this.model.settings.get('lineColor'),
                fill: this.model.settings.get('backgroundColor')
            });
            element.set('elementSettings', elementSettings);
            element.set('position', this.model.elements.length);
            element.set('hash', this.model.get('hash'));
            this.model.elements.add(element);

            element.url = 'elements/' + element.get('hash');
            element.save()
                .done(function (data) {
                    element.set('id', data._id);
                })
        }
    });
});