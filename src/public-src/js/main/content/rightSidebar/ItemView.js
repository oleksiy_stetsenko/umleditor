define([
    'backbone.marionette',
    './template',
    '../../../libs/jquery',
    '../../../libs/bootstrap-colorpicker.min',
    '../../../libs/selectize.min',
    '../../../libs/bootstrap.min'
], function (Marionette, template) {
    return Marionette.ItemView.extend({
        el: '.right-bar',
        template: template,

        ui: {
            colorBg: '#bg-color-rb',
            colorLine: ('#line-color-rb'),

            selectLineType: '#select-line-type-rb',
            paperSize: '#paper-size-rb',

            lineColor: '#color-line-rb',
            bgColor: '#color-bg-rb',
            lineType: '#select-line-type-rb',
            tooltip: '[data-toggle="tooltip"]',

            elementSettings: '.element'
        },

        events: {
            'change @ui.lineType': 'changeLineType',
            'changeColor @ui.colorLine': 'changeLineColor',
            'changeColor @ui.colorBg': 'changeBackgroundColor',
            'change @ui.elementSettings': 'updateElementSettings'

        },

        initialize: function (options) {
            this.on('render', this.onShow);
            this.render();
            this.docModel = options.docModel;
        },

        onShow: function () {
            this.ui.colorLine.colorpicker({customClass: 'elemPicker'});
            this.ui.colorBg.colorpicker({customClass: 'elemPicker'});
            this.ui.selectLineType.selectize();
            this.ui.tooltip.tooltip();
        },

        modelChange: function () {
            this.currentModel().trigger('updateElement');
        },

        currentModel: function () {
                return this.docModel.elements.findWhere({select: 'current'});
        },

        updateElementSettings: function (item) {
            var obj = this.currentModel().get('elementSettings');
            for (var property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (property === item.target.id) {
                        var s = this.ui.elementSettings;
                        for (var uiProp in s) {
                            if (s.hasOwnProperty(uiProp)) {
                                if (s[uiProp].id === item.target.id) {
                                    this.currentModel().get('elementSettings')[property] = s[uiProp].value;
                                }
                            }
                        }
                    }
                }
            }
            this.modelChange();
        },

        destroy: function () {
            this.$el.empty();
        },

        onBeforeDestroy: function () {
            this.ui.colorLine.colorpicker('destroy');
            this.ui.colorBg.colorpicker('destroy');
        },

        changeLineType: function () {
            this.currentModel().get("generalSettings").strokeDasharray = this.ui.selectLineType.val();
            this.modelChange();
        },

        changeLineColor: function () {
            this.currentModel().get("generalSettings").stroke = this.ui.lineColor.val();
            this.modelChange();
        },

        changeBackgroundColor: function () {
            this.currentModel().get("generalSettings").fill = this.ui.bgColor.val();
            this.modelChange();
        }
    });
});
