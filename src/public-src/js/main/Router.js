define([
    'backbone',
    './../document/Model',
    './header/LayoutView',
    './header/mainButtons/ItemView',
    './header/docSettings/ItemView',
    './content/LayoutView',
    './content/contentSvg/CollectionView',
    './content/leftSidebar/ItemView',
    './content/rightSidebar/ItemView',
    './header/mainButtons/modal/CollectionView'


], function (Backbone, DocModel, LayoutHeader, MainButtonView, SettingsView,
             LayoutContent, CollectionView, LeftSidebarView, RightSidebarView, ModalView) {
    return Backbone.Router.extend({

        initialize: function (options) {
            this.container = options.container;
        },

        routes: {
            '': 'index'
        },

        index: function () {

            var docModel = new DocModel;

            var header = new LayoutHeader;
            var content = new LayoutContent;

            this.container.header.show(header);
            this.container.content.show(content);

            var mainButtonView = new MainButtonView({
                container: header.modal,
                model: docModel
            });
            header.mainButtons.show(mainButtonView);

            var modalView = new ModalView({
                model: docModel,
                collection: mainButtonView.collection
            });
            header.modal.show(modalView);

            var settingsView = new SettingsView({model: docModel.settings});
            header.docSet.show(settingsView);

            var leftSidebarView = new LeftSidebarView({model: docModel});
            content.leftBar.show(leftSidebarView);
        }
    });
});