define([
    'backbone',
    './settings/Model',
    './elements/Collection'
], function (Backbone, Settings, Elements) {
    return Backbone.Model.extend({
        idAttribute: "hash",

        urlRoot: '/document',

        defaults: {
            title: 'New Document'
        },

        initialize: function () {
            this.elements = new Elements;
            this.settings = new Settings;
        },

        toJSON: function() {
            var json = Backbone.Model.prototype.toJSON.call(this);
            json.elements = this.elements.toJSON();
            json.settings = this.settings.toJSON();
            json.title = this.get('title');
            return json;
        },

        parse: function (response) {

            //todo: this line fixed clone same events(reWright this)
            if (!_.isUndefined(this.elements._events)) {
                this.elements._events.add = this.elements._events.add.slice(0, 1);
            }


            if (response.settings) {
                this.settings.attributes = response.settings;
                this.settings.set('hash', response.hash);
                delete response.settings;
            }

            if (Array.isArray(response.elements)) {
                response.elements.forEach(function (item) {
                    item.id = item._id;
                    item.hash = response.hash;
                    this.elements.add(item);
                }.bind(this));
                delete response.elements;
            }

            return response;
        },

        getElementDefaults: function () {
            return {
                ellipse: {
                    cx: 90,
                    cy: 60,
                    rx: 70,
                    ry: 40
                },
                rect: {
                    x: 20,
                    y: 20,
                    width: 150,
                    height: 100
                },
                line: {
                    x1: 30,
                    y1: 100,
                    x2: 100,
                    y2: 30
                }
            }
        }
    });
});