define([
    'backbone',
    './settings/Model'

], function (Backbone, ModelElement) {
    return Backbone.Collection.extend({
        model: ModelElement,
        url: '/document',

        initialize: function () {
            this.fetch();
        }
    });
});
