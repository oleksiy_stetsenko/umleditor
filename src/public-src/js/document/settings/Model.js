define([
    'backbone'
], function (Backbone) {
    return Backbone.Model.extend({
        idAttribute: "hash",
        defaults: {
            width: 583,
            height: 825,
            lineType: 'solid',
            lineColor: '#1d1f1c',
            backgroundColor: '#ccc'
        },
        urlRoot: '/settings',

        getDefaultValue: function () {
            return {
                width: 583,
                height: 825,
                lineType: 'solid',
                lineColor: '#1d1f1c',
                backgroundColor: '#ccc'
            };
        }

    });
});