define([
    'backbone',
    './Model'

], function (Backbone, ModelElement) {
    return Backbone.Collection.extend({
        model: ModelElement
    });
});
