var
    gulp = require('gulp'),
    gulpsync = require('gulp-sync')(gulp),
    del = require('del'),
    uglify = require('gulp-uglify'),
    print = require('gulp-print'),
    watch = require('gulp-watch'),
    jade = require('gulp-jade'),
    wrap = require('gulp-wrap'),
    rename = require('gulp-rename'),
    path = require('path');

var LIBS = [
    './node_modules/requirejs/require.js',
    './node_modules/jquery/dist/jquery.js',
    './node_modules/jade/runtime.js',
    './node_modules/backbone/backbone.js',
    './node_modules/backbone.marionette/lib/backbone.marionette.js',
    './node_modules/backbone.localstorage/backbone.localstorage.js',
    './node_modules/underscore/underscore.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
    './node_modules/selectize/dist/js/standalone/selectize.min.js'
];

gulp.task('build:static:node_modules', function () {
    return gulp.src(LIBS)
        .pipe(rename(function (path) {
            if (path.basename === 'runtime') {
                path.basename = 'jade';
            }
        }))
        .pipe(gulp.dest('src/public/js/libs/'));
});

gulp.task('build:clear', function () {
    return del(['./src/public/**']);
});

gulp.task('build:js', function () {
    return gulp.src('./src/public-src/**/*.js')
        .pipe(print(function (path) {
            return 'build:js => ' + path;
        }))
        //.pipe(uglify())
        .pipe(gulp.dest('./src/public/'));
});

gulp.task('build:css', function () {
    return gulp.src('./src/public-src/styles/**')
        .pipe(print(function (path) {
            return 'build:css => ' + path;
        }))
        .pipe(gulp.dest('./src/public/styles'));
});

gulp.task('build:jade', function () {
    return gulp.src('./src/public-src/**/*.jade')
        .pipe(jade({client: true}))
        .pipe(wrap('define(["libs/jade"], function(jade) {return <%= contents %>})'))
        .pipe(uglify())
        //.pipe(rename({suffix: '.jade'}))
        .pipe(gulp.dest('./src/public/'));
});

gulp.task('static:images', function () {
    return gulp.src('./src/public-src/images/**')
        .pipe(print(function (path) {
            return 'static:images => ' + path;
        }))
        .pipe(gulp.dest('./src/public/images/'));
});

gulp.task('build:static', ['static:images', 'build:static:node_modules']);

gulp.task('build', gulpsync.sync(['build:clear', ['build:js', 'build:css', 'build:jade', 'build:static']]));

gulp.task('watch', ['build'], function () {
    watch(['./src/**/*', '!./src/public/*'], function () {
        gulp.start('build');
    });
});

gulp.task('default', ['build']);

/// DEPLOY

gulp.task('deploy:clear', function () {
    return del(['deploy/*', '!deploy/.git/']);
});

gulp.task('deploy:data', function () {
    return gulp.src(['./src/data/**/*'])
        .pipe(gulp.dest('./deploy/data'));
});

gulp.task('deploy:public', function () {
    return gulp.src(['./src/public/**/*'])
        .pipe(gulp.dest('./deploy/public'));
});

gulp.task('deploy:routes', function () {
    return gulp.src(['./src/routes/**/*'])
        .pipe(uglify())
        .pipe(gulp.dest('./deploy/routes'));
});

gulp.task('deploy:mongo', function () {
    return gulp.src(['./src/mongo/**/*'])
        .pipe(uglify())
        .pipe(gulp.dest('./deploy/mongo'));
});

gulp.task('deploy:views', function () {
    return gulp.src(['./src/views/**/*'])
        .pipe(gulp.dest('./deploy/views'));
});

gulp.task('deploy:common', function () {
    return gulp.src(['./src/*.js', './package*.json'])
        .pipe(gulp.dest('./deploy/'));
});

gulp.task('deploy:copy', [
    'deploy:data',
    'deploy:public',
    'deploy:routes',
    'deploy:mongo',
    'deploy:views',
    'deploy:common'
]);

gulp.task('deploy', gulpsync.sync(['deploy:clear', ['deploy:copy']]));
